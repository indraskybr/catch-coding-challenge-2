<?php

namespace App\Util;

interface ProductEntityInterface
{
    public function getId(): ?string;

    public function setId(string $id);

    public function getName(): ?string;

    public function setName(string $name);

    public function getSalePrice(): ?int;

    public function setSalePrice(int $sale_price);

    public function getRetailPrice(): ?int;

    public function setRetailPrice(?int $retail_price);

    public function getImageUrl(): ?string;

    public function setImageUrl(?string $image_url);

    public function getQuantityAvailable(): ?int;
    
    public function setQuantityAvailable(int $quantity_available);
}