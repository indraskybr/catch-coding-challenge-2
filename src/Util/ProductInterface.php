<?php

namespace App\Util;

interface ProductInterface
{
    public function queryProduct();
    
    public function fetchAll(): ?array;

    public function fetch($id): ?array;

    public function prepareProduct($product): array;
}