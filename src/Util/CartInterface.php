<?php

namespace App\Util;

interface CartInterface
{
    public function createCart($id): array;

    public function checkCartId($id): bool;

    public function getCartsInSession(): ?array;

    public function getCart($id): array;

    public function updateCart($cart, $product, $quantity): bool;

    public function updateCartSession($updatedCart);

    public function setItems($cart, $product, $quantity): array;

    public function calculateSalePrice($items): int;

    public function calculateRetailPrice($items): int;

    public function calculateSavings($salePrice, $retailPrice): int;

    public function formatAmount($amount): string;
}