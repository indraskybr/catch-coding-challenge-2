<?php

namespace App\Util;

interface CartItemInterface 
{
    public function isProductExists($cart, $product): bool;

    public function getProduct($id): ?array;
}