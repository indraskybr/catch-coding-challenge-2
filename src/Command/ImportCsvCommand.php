<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Products;
use League\Csv\Reader;

class ImportCsvCommand extends Command
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct();

        $this->em = $em; 
    }

    protected function configure()
    {
        $this->setName('csv:import')
             ->setDescription('Import CSV file');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        
        $io->title('Starting...');

        $reader = Reader::createFromPath('%kernel.root_dir%/../src/Data/products.csv');
        $reader->setHeaderOffset(0);
        $records = $reader->getRecords();

        foreach ($records as $offset => $row) {
            $products = (new Products())
            ->setId($row['id'])
            ->setName($row['name'])
            ->setSalePrice($row['sale_price'])
            ->setRetailPrice($row['rrp'] == '' ? null : $row['rrp'])
            ->setImageUrl($row['image_url'])
            ->setQuantityAvailable($row['quantity_available']);

            $this->em->persist($products);
        }

        $this->em->flush();

        $io->success('Done!');
    }
}