<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use App\Service\ProductService;
use App\Service\CartService;

class DefaultController 
{
    /**
     * @Route("/products", methods={"GET"})
     */
    public function list(ProductService $product)
    {
        $response = $product->fetchAll();
        return new JsonResponse($response);
    }

    /**
     * @Route("/carts/{id}", methods={"POST"})
     */
    public function createCarts(CartService $cartService, Request $request)
    {
        $cartId = $request->get('id');

        $isCartIdCreated = $cartService->checkCartId($cartId);

        if ($isCartIdCreated) {
            $getCart = $cartService->getCart($cartId);
            $newCart = $getCart['data'];
        } else {
            $newCart = $cartService->createCart($cartId);
        }

        $newCart = ['cart' => $newCart];
        return new JsonResponse($newCart);
    }

    /**
     * @Route("/carts/{id}", methods={"GET"})
     */
    public function getCarts(CartService $cartService, Request $request)
    {
        $cartId = $request->get('id');
        $cart = $cartService->getCart($cartId);

        if ($cart['status']) {
            $cart = ['cart' => $cart['data']];
            return new JsonResponse($cart);
        } else {
            throw new BadRequestHttpException($cart['data'], null, 404);
        }
    }

    /**
     * @Route("/carts/{id}/updateItems", methods={"POST"})
     */
    public function updateItems(CartService $cartService, Request $request)
    {
        $cartId = $request->get('id');
        $productId = $request->get('productId');
        $quantity = (integer) $request->get('quantity');

        $product = $cartService->getProduct($productId);

        $isCartIdCreated = $cartService->checkCartId($cartId);

        if ($product == null) {
            throw new BadRequestHttpException('Invalid Product', null, 400); 
        }

        if ($quantity > $product['quantityAvailable'] || !$isCartIdCreated) {
            throw new BadRequestHttpException('Error updating Cart items', null, 400); 
        }

        $cart = $cartService->getCart($cartId)['data'];
        $updateCart = $cartService->updateCart($cart, $product, $quantity);

        if ($updateCart == false) {
            throw new BadRequestHttpException('Error updating Cart items', null, 400); 
        }

        return new JsonResponse('Successfully updated the cart items');
    }
}
