<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Util\ProductEntityInterface;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProductsRepository")
 */
class Products implements ProductEntityInterface
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="string", length=36)
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="integer")
     */
    private $sale_price;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $retail_price;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $image_url;

    /**
     * @ORM\Column(type="integer")
     */
    private $quantity_available;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function setId(string $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getSalePrice(): ?int
    {
        return $this->sale_price;
    }

    public function setSalePrice(int $sale_price): self
    {
        $this->sale_price = $sale_price;

        return $this;
    }

    public function getRetailPrice(): ?int
    {
        return $this->retail_price;
    }

    public function setRetailPrice(?int $retail_price): self
    {
        $this->retail_price = $retail_price;

        return $this;
    }

    public function getImageUrl(): ?string
    {
        return $this->image_url;
    }

    public function setImageUrl(?string $image_url): self
    {
        $this->image_url = $image_url;

        return $this;
    }

    public function getQuantityAvailable(): ?int
    {
        return $this->quantity_available;
    }

    public function setQuantityAvailable(int $quantity_available): self
    {
        $this->quantity_available = $quantity_available;

        return $this;
    }
}
