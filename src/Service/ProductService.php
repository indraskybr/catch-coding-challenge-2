<?php

namespace App\Service;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Entity\Products;
use App\Util\ProductInterface;

class ProductService extends AbstractController implements ProductInterface
{
    public function queryProduct()
    {
        return $this->getDoctrine()->getRepository(Products::class);
    }

    public function fetchAll(): ?array
    {
        $list = array();

        $products = $this->queryProduct()->findAll();

        foreach ($products as $product) {
            $list[] = $this->prepareProduct($product);
        }

        return $list;
    }

    public function fetch($id): ?array
    {
        $product = $this->queryProduct()->find($id);

        if (!$product) {
            return null;
        }

        $product = $this->prepareProduct($product);

        return $product;
    }

    public function prepareProduct($product = null): array
    {
        if ($product != null) {
            $product = [
                'id' => $product->getId(),
                'name' => $product->getName(),
                'salePrice' => $product->getSalePrice() == null ? 0 : $product->getSalePrice(),
                'retailPrice' => $product->getRetailPrice() == null ? 0 : $product->getRetailPrice(),
                'imageUrl' => $product->getImageUrl(),
                'quantityAvailable' => $product->getQuantityAvailable()
            ];
        }

        return $product;
    }
}
