<?php

namespace App\Service;

use Symfony\Component\HttpFoundation\Session\SessionInterface;

use App\Util\CartInterface;
use App\Util\CartItemInterface;
use App\Util\ProductInterface;

class CartService implements CartInterface, CartItemInterface
{
   protected $product;
   protected $carts = array();
   private $session;

   public function __construct(ProductInterface $product, SessionInterface $session)
   {
       $this->product = $product;
       $this->session = $session;

       // set some base values 
       $this->carts = [
           'id' => '',
           'items' => [],
           'total' => 0,
           'totalFormatted' => $this->formatAmount(0),
           'savings' => 0,
           'savingsFormatted' => $this->formatAmount(0)
       ]; 
   }

   public function createCart($id): array
   {
       $newCart = $this->carts;
       $newCart['id'] = $id;

       $carts = $this->getCartsInSession();

       if (count($carts)) {
           array_push($carts, $newCart);
       } else {
           $carts[] = $newCart;
       }

       $this->session->set('cart', $carts);

       return $newCart;
   }

   public function checkCartId($id): bool
   {
       $carts = $this->getCartsInSession();
       
       if ($carts != null) {
           foreach ($carts as $cart) {
               if ($id == $cart['id']) {
                   return true;
               } 
           }
       }

       return false;
   }

   public function getCartsInSession(): ?array
   {
       return $this->session->get('cart');
   }

   public function getCart($id): array
   {
       $isCartIdCreated = $this->checkCartId($id);

       if ($isCartIdCreated) {
           $carts = $this->getCartsInSession();
           foreach ($carts as $cart) {
               if ($id == $cart['id']) {
                   $getCart = $cart;
                   break;
               } 
           }
           return [
               'status' => true,
               'data' => $getCart
           ];
       } else {
           return [
               'status' => false,
               'data' => 'Invalid or missing Cart id'
           ];
       }
   }

   public function updateCart($cart, $product, $quantity): bool
   {
       try {
           $updatedCart = $this->setItems($cart, $product, $quantity);

           $salePrice = $this->calculateSalePrice($updatedCart['items']);
           $retailPrice = $this->calculateRetailPrice($updatedCart['items']);
           $savings = $this->calculateSavings($salePrice, $retailPrice);

           $updatedCart['total'] = $salePrice;
           $updatedCart['totalFormatted'] = $this->formatAmount($salePrice);
           $updatedCart['savings'] = $savings;
           $updatedCart['savingsFormatted'] = $this->formatAmount($savings);
           $this->updateCartSession($updatedCart);
           return true;
       } catch (\Exception $e) {
           return false;
       }
   }

   public function updateCartSession($updatedCart)
   {
       $carts = $this->getCartsInSession();
       for ($i=0;$i<count($carts);$i++) {
           if ($updatedCart['id'] == $carts[$i]['id']) {
               $carts[$i] = $updatedCart;
               break;
           } 
       }
       $this->session->set('cart', $carts);
   }

   public function setItems($cart, $product, $quantity): array
   {
       $check = $this->isProductExists($cart, $product);

       if ($check) {
           for ($i=0;$i<count($cart['items']);$i++) {
               if ($cart['items'][$i]['product']['id'] == $product['id']) {
                   if ($quantity > 0) {
                       $cart['items'][$i]['quantity'] = $quantity;
                   } else {
                       unset($cart['items'][$i]);
                   }
               }
           }
       } else {
           $item = [
               'quantity' => $quantity,
               'product' => $product
           ];
           array_push($cart['items'], $item);
       }

       return $cart;
   }

   public function isProductExists($cart, $product): bool
   {
       if ($cart != null || $product != null) {
           foreach ($cart['items'] as $item) {
               if ($item['product']['id'] == $product['id']) {
                   return true;
               }
           }
       }
       return false;
   }

   public function getProduct($id): ?array
   {
       $product = $this->product->fetch($id);

       return $product;
   }

   public function calculateSalePrice($items): int
   {
       $total = 0;
       foreach ($items as $item) {
           $quantity = $item['quantity'];
           $salePrice = $item['product']['salePrice'] == null ? 0 : $item['product']['salePrice'];
           $total = $total + ($quantity * $salePrice);
       }
       return $total;
   }

   public function calculateRetailPrice($items): int 
   {
       $total = 0;
       foreach ($items as $item) {
           $quantity = $item['quantity'];
           $retailPrice = $item['product']['retailPrice'] == null ? 0 : $item['product']['retailPrice'];
           $total = $total + ($quantity * $retailPrice);
       }
       return $total;
   }

   public function calculateSavings($salePrice, $retailPrice): int
   {
       if ($retailPrice > $salePrice) {
           return $retailPrice - $salePrice;
       } else {
           return 0;
       }
   }

   public function formatAmount($amount): string 
   {
       return '$'.number_format(($amount /100), 2, '.', ' ');
   }
}
