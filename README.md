# Catch Coding Challenge 2

This is a simple shopping cart project.

To begin, in command line type `composer install`, this will install all the dependencies for this project.

Connect to a clean database and run `php bin/console doctrine:migration:migrate` to create all the necessary tables.

To import data from the product.csv, run the `php bin/console csv:import`.

This project includes:

 - `"/products", methods={"GET"}`
 
 - `"/carts/{id}", methods={"POST"}`
 
 - `"/carts/{id}", methods={"GET"}`
 
 - `"/carts/{id}/updateItems", methods={"POST"}`
 
 

## Contact
If you have any question, feel free to drop a message. Cheers!

ALBERT TANAGA  
albert.tanaga@gmail.com